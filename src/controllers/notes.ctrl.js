const { Note } = require('../models');
const notesCtrl = {};

notesCtrl.getNote = async (req, res) => {
    const notes = await Note.findById(req.params.id);
    res.json(notes);
}

notesCtrl.getNotes = async (req, res) => {
    const notes = await Note.find();
    res.json(notes);
}

notesCtrl.createNote = async (req, res) => {
    const { title, content, author, date } = req.body;
    const newNote = new Note({title, content, author, date});
    const noteSaved = await newNote.save();
    res.json({msg: 'note saved', noteSaved});
}

notesCtrl.deleteNote = async (req, res) => {
    await Note.findByIdAndDelete(req.params.id);
    res.json({msg: 'note deleted'});
}

notesCtrl.updateNote = async (req, res) => {
    await Note.findByIdAndUpdate(req.params.id, req.body);
    res.json({msg: 'note updated'});
}

module.exports = notesCtrl;