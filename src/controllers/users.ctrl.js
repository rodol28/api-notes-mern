const { User } = require('../models');
const usersCtrl = {};

usersCtrl.getUsers = async (req, res) => {
    const users = await User.find();
    res.json(users);
}

usersCtrl.createUser = async (req, res) =>{
    const { username } = req.body;
    const newUser = new User({username});
    const userSaved = await newUser.save()
    res.json({msg: 'user created', userSaved});
}


usersCtrl.deleteUser = async (req, res) => {
    await User.findByIdAndDelete(req.params.id);
    res.json({msg: 'user deleted'});
}

module.exports = usersCtrl;