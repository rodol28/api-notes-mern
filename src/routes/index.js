module.exports = {
    usersRoutes: require('./users.routes'),
    notesRoutes: require('./notes.routes')
}