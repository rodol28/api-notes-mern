const router = require('express').Router();
const { notesCtrl } = require('../controllers');

router.route('/')
    .get(notesCtrl.getNotes)
    .post(notesCtrl.createNote);

router.route('/:id')
    .delete(notesCtrl.deleteNote)
    .put(notesCtrl.updateNote)
    .get(notesCtrl.getNote);

module.exports = router; 