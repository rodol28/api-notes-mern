const router = require('express').Router();
const { usersCtrl } = require('../controllers');

router.route('/')
    .get(usersCtrl.getUsers)
    .post(usersCtrl.createUser);

router.route('/:id')
    .delete(usersCtrl.deleteUser);

module.exports = router;