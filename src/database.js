const mongoose = require('mongoose');

const URI = process.env.DATABASE ? 
                process.env.DATABASE 
                : 'mongodb://localhost/databasetest';

mongoose.connect(URI, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
})
.then(db => console.log('db is connected'))
.catch(err => console.error(new Error('Failed to connect to database')));