const dotenv = require('dotenv');
dotenv.config();
const express = require('express');
const cors = require('cors');
require('./database');
const app = express();
const {notesRoutes, usersRoutes} = require('./routes');

// settings
app.set('port', process.env.PORT || 3000);

// middleware
app.use(cors());
app.use(express.json());

// routes
app.use('/api/notes', notesRoutes);
app.use('/api/users', usersRoutes);

module.exports = app;